#!/bin/bash

echo "Atualizando o sistema"
sudo apt update
sudo apt upgrade -y

echo "Instalando pacotes que serão usados para outras instalações"
sudo apt-get install apt-transport-https ca-certificates curl gnupg lsb-release jq wget -y 

echo "Instalando alguns terminais (terminator, tilix e zsh)"
sudo apt install terminator tilix zsh -y 

echo "Mudando o terminal padrão para o zsh"
sudo chsh -s /bin/zsh

cd /tmp
echo "Configurando o git e gitflow"
git clone https://gitlab.com/davidpuziol/tunning-git.git
sudo chmod u+x ./tunning-git/gitconfig.sh
sudo ./tunning-git/gitconfig.sh
rm -rf tunning-git/
sudo apt-get install git-flow -y

echo "Instalando fontes firacode e meslo" 
wget https://github.com/tonsky/FiraCode/releases/download/5.2/Fira_Code_v5.2.zip
unzip Fira_Code_v5.2.zip -d Fira_Code_v5.2
sudo mkdir /usr/share/fonts/firacode
sudo mv Fira_Code_v5.2/ttf/* /usr/share/fonts/firacode
rm -rf Fira_Code_v5.2*

wget https://github.com/andreberg/Meslo-Font/raw/master/dist/v1.2.1/Meslo%20LG%20DZ%20v1.2.1.zip
sudo mkdir /usr/share/fonts/meslo
unzip Meslo\ LG\ DZ\ v1.2.1.zip -d Meslo\ LG\ DZ\ v1.2.1
sudo mv Meslo\ LG\ DZ\ v1.2.1/* /usr/share/fonts/meslo
rm -rf Meslo\ LG\ DZ\ v1.2.1*

echo "Personalizando o ZSH"
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" -y
wget https://github.com/dracula/gnome-terminal/archive/refs/heads/master.zip
unzip master.zip
./gnome-terminal-master/install.sh
rm -rf master.zip
rm -rf gnome-terminal.master
sudo git clone https://github.com/denysdovhan/spaceship-prompt.git "$ZSH_CUSTOM/themes/spaceship-prompt"
sudo ln -s "$ZSH_CUSTOM/themes/spaceship-prompt/spaceship.zsh-theme" "$ZSH_CUSTOM/themes/spaceship.zsh-theme"
sed -i 's/robbyrussell/spaceship/g' ~/.zshrc
yes | sh -c "$(curl -fsSL https://raw.githubusercontent.com/zdharma/zinit/master/doc/install.sh)"

echo "SPACESHIP_PROMPT_ORDER=(
  user          # Username section
  dir           # Current directory section
  host          # Hostname section
  git           # Git section (git_branch + git_status)
  hg            # Mercurial section (hg_branch  + hg_status)
  exec_time     # Execution time
  line_sep      # Line break
  vi_mode       # Vi-mode indicator
  jobs          # Background jobs indicator
  exit_code     # Exit code section
  char          # Prompt character
)
SPACESHIP_USER_SHOW=always
SPACESHIP_PROMPT_ADD_NEWLINE=false
SPACESHIP_CHAR_SYMBOL="❯"
SPACESHIP_CHAR_SUFFIX=" "" >> ~/.zshrc

echo "zinit light zdharma/fast-syntax-highlighting
zinit light zsh-users/zsh-autosuggestions
zinit light zsh-users/zsh-completions" >> ~/.zshrc

echo "Instalando o chrony"
sudo apt-get remove 'ntp*'
sudo apt-get install chrony -y
sudo sed -i "/server.*/c\server 169.254.169.123 prefer iburst" /etc/chrony/chrony.conf 
sudo systemctl start chrony
sudo systemctl enable chrony

cd ~
#echo "Gerando um chave SSH"
#yes | ssh-keygen -t ed25519
echo "Criando pasta de icones ocultas"
git clone https://gitlab.com/davidpuziol/my-desktop-icons.git .icons

echo "Melhorando o Gnome"
sudo apt install gnome-tweaks -y

echo "Instalando o Homebrew"
sudo apt-get install build-essential curl file git -y
echo | /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
test -d ~/.linuxbrew && eval $(~/.linuxbrew/bin/brew shellenv)
test -d /home/linuxbrew/.linuxbrew && eval $(/home/linuxbrew/.linuxbrew/bin/brew shellenv)
echo "eval $($(brew --prefix)/bin/brew shellenv)" >>~/.profile

echo "Instalando o Snapd"
sudo apt-get install snapd -y
sudo snap install whatsapp-for-linux
#sudo snap refresh whatsapp-for-linux

echo "Instalando o Figma"
sudo add-apt-repository ppa:chrdevs/figma -y
sudo apt update
sudo apt install figma-linux -y
sudo chmod +x /usr/local/bin/figma-linux

echo "Instalando o Vim"
sudo apt install vim -y

echo "Instalando o VScode"
wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg
sudo install -o root -g root -m 644 packages.microsoft.gpg /etc/apt/trusted.gpg.d/
sudo sh -c 'echo "deb [arch=amd64,arm64,armhf signed-by=/etc/apt/trusted.gpg.d/packages.microsoft.gpg] https://packages.microsoft.com/repos/code stable main" > /etc/apt/sources.list.d/vscode.list'
rm -f packages.microsoft.gpg
sudo apt update
sudo apt install code -y

echo "Instalando o JetBrains Tool Box"
wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg
cd /tmp
wget https://download.jetbrains.com/toolbox/jetbrains-toolbox-1.21.9712.tar.gz\?_gl\=1\*7cvh4\*_ga\*ODc3MjYwNDkuMTYzMjcxNDc2OA..\*_ga_V0XZL7QHEB\*MTYzMjcxNTMwNS4xLjEuMTYzMjcxNTQ4Ny4w\&_ga\=2.257773323.1820575390.1632714768-87726049.1632714768
sudo tar -xzf jetbrains* -C /opt
sudo rm -Rf /tmp/jetbrains*
sudo /opt/jetbrains-toolbox-1.21.9712/jetbrains-toolbox bash
cd ~


echo "Instalando o Chrome"
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
sudo apt install ./google-chrome-stable_current_amd64.deb -y
rm -rf google-chrome-stable_current_amd64.deb 

echo "Instalando VLC"
sudo apt install vlc -y

echo "Instalando o Slack"
#sudo apt install slack-desktop -y
sudo snap install slack --classic

echo "Instalando pacotes para desenvolvimento"
echo "Instalando o Docker"
sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo \
  "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt update
sudo apt-get install docker-ce docker-ce-cli containerd.io -y
sudo usermod -aG docker $USER
sudo chown $USER /var/run/docker.sock

echo "Instalando o Docker-Compose"
sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

echo "Instalando Kubernetes Kubectl"
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
curl -LO "https://dl.k8s.io/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl.sha256"
echo "$(<kubectl.sha256) kubectl" | sha256sum --check
sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl

echo "Instalando Kubernetes EKSctl"
curl --silent --location "https://github.com/weaveworks/eksctl/releases/latest/download/eksctl_$(uname -s)_amd64.tar.gz" | tar xz -C /tmp
sudo mv /tmp/eksctl /usr/local/bin
mkdir -p ~/.zsh/completion/
eksctl completion zsh > ~/.zsh/completion/_eksctl
fpath=($fpath ~/.zsh/completion)
echo "autoload -U compinit" | zsh #problema aqui
echo "compinit" | zsh #problema aqui

echo "Instalando o Nodejs, NPM e Yarn"
sudo apt-get install nodejs -y
sudo apt-get install npm -y
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt update
sudo apt-get install yarn -y

echo "Instalando o NVM"
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.38.0/install.sh | zsh

echo "Instalando Docusaurus"
sudo npm install -g docusaurus-init
echo "Instalando Surge"
sudo npm install -g surge
echo "Instalando Broken Link checker"
sudo npm install -g broken-link-checker
echo "Instalando o Snyk"
sudo npm install -g snyk

echo "Instalando o OpenJava"
sudo apt install default-jre -y
sudo apt install default-jdk -y

echo "Instalando o .Net"
wget https://packages.microsoft.com/config/ubuntu/21.04/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
sudo dpkg -i packages-microsoft-prod.deb
rm packages-microsoft-prod.deb
sudo apt-get update; \
  sudo apt-get install -y apt-transport-https && \
  sudo apt-get update && \
  sudo apt-get install -y dotnet-sdk-5.0
sudo apt-get install -y dotnet-runtime-5.0
#sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys EA8CACC073C3DB2A
#yes | sudo add-apt-repository ppa:linuxuprising/java
#sudo apt update

echo "Instalando o Tfswitch"
curl -L https://raw.githubusercontent.com/warrensbox/terraform-switcher/release/install.sh | sudo zsh
tfswitch -u

echo "Instalando o Terraform, Terragrunt e Terraform-docs"
#terraform -install-autocomplete
sudo apt install graphviz -y   ## analisar todo o terraform
brew install terragrunt
brew install terraform-docs

echo "Instalando o Packer"
LATEST_URL=$(curl -sL https://releases.hashicorp.com/packer/index.json | jq -r '.versions[].builds[].url' | sort -n | egrep -v 'rc|beta' | grep "linux_amd64" |tail -1)
curl ${LATEST_URL} > /tmp/packer.zip
sudo unzip /tmp/packer.zip
sudo mv packer /usr/local/bin/packer
sudo chmod u+x packer
rm -rf /tmp/packer.zip

echo "Instalando o Ansible"
sudo apt-get update
sudo apt-get install software-properties-common -y
sudo add-apt-repository --yes --update ppa:ansible/ansible
sudo apt-get install ansible -y

echo "Instalando AWS-CLI"
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
sudo ./aws/install
rm -rf aws*

echo "Instalando AWS-IAM-AUTHENTICATOR"
curl -o aws-iam-authenticator https://amazon-eks.s3.us-west-2.amazonaws.com/1.21.2/2021-07-05/bin/linux/amd64/aws-iam-authenticator
chmod +x ./aws-iam-authenticator
sudo mv aws-iam-authenticator /usr/local/bin

echo "Instalando AZURE-CLI"
curl -sL https://packages.microsoft.com/keys/microsoft.asc |
    gpg --dearmor |
    sudo tee /etc/apt/trusted.gpg.d/microsoft.gpg > /dev/null
AZ_REPO=$(lsb_release -cs)
echo "deb [arch=amd64] https://packages.microsoft.com/repos/azure-cli/ $AZ_REPO main" |
    sudo tee /etc/apt/sources.list.d/azure-cli.list
sudo apt-get update
sudo apt-get install azure-cli -yazu -y

echo "Instalando GParted"
sudo apt install gparted -y

echo "Instalando o OBS"
sudo apt-get install obs-studio -y

echo "Instalando o Telegram"
sudo wget -O- https://telegram.org/dl/desktop/linux | sudo tar xJ -C /opt/
sudo ln -s /opt/Telegram/Telegram /usr/local/bin/telegram-desktop

echo "Instalando o Wireguard"
sudo apt-get install wireguard -y

echo "Instalando o Transmission"
yes | sudo add-apt-repository ppa:transmissionbt/ppa
sudo apt-get update
sudo apt-get install transmission transmission-qt -y

echo "Instalando insomnia"
echo "deb [trusted=yes arch=amd64] https://download.konghq.com/insomnia-ubuntu/ default all" | sudo tee -a /etc/apt/sources.list.d/insomnia.list
sudo apt-get update
sudo apt-get install insomnia -y

echo "Instalando Postman"
wget https://dl.pstmn.io/download/latest/linux64
sudo tar -xvf linux64 -C /usr/bin
export PATH=$PATH:/usr/bin/Postman
sudo touch /usr/share/applications/postman.desktop
echo "[Desktop Entry]
Name=Postman API Tool
GenericName=Postman
Comment=Testing API
Exec=/usr/bin/Postman/Postman
Terminal=false
X-MultipleArgs=false
Type=Application
Icon=/usr/bin/Postman/app/resources/app/assets/icon.png
StartupWMClass=Postman
StartupNotify=true" >> /usr/share/applications/postman.desktop

echo "Instalando o Discord"
sudo apt-get install gdebi-core -y
wget -O ~/discord.deb "https://discordapp.com/api/download?platform=linux&format=deb"
sudo gdebi -n ~/discord.deb 
sudo rm ~/discord.deb

echo "Instalando o Spotify"
sudo sh -c "echo 'deb http://repository.spotify.com stable non-free' >> /etc/apt/sources.list.d/spotify.list"
sudo apt install dirmngr -y
curl -sS https://download.spotify.com/debian/pubkey_0D811D58.gpg | sudo apt-key add -
sudo apt-get update
sudo apt-get install spotify-client -y


echo "Instalando o Cura Arachnne Beta"
wget https://github.com/Ultimaker/Cura/releases/download/Arachne_engine_beta/Ultimaker_Cura-Arachne_engine_beta.AppImage
sudo mv Ultimaker_Cura* /usr/local/bin/cura.AppImage
sudo chmod +x /usr/local/bin/cura.AppImage 
touch ~/.local/share/applications/cura.desktop
echo "[Desktop Entry]
Name=Cura
Comment=3D Printing Software
Exec=/usr/local/bin/cura.AppImage
Icon=/home/$USER/.icons/cura.png
Terminal=false
Type=Application
Categories=Development" >> ~/.local/share/applications/cura.desktop

echo "Instalando o Notion"
wget https://notion.davidbailey.codes/notion-linux.list
sudo mv notion-linux.list /etc/apt/sources.list.d/notion-linux.list
sudo apt update 
sudo apt install notion-desktop -y

echo "Instalando o AnyDesk"
wget -qO - https://keys.anydesk.com/repos/DEB-GPG-KEY | sudo apt-key add -
echo "deb http://deb.anydesk.com/ all main" | sudo tee /etc/apt/sources.list.d/anydesk-stable.list
sudo apt-get update
sudo apt-get install anydesk -y

echo "Instalando o Gnome Boxes"
sudo apt-get update
sudo apt-get install gnome-boxes -y

echo "Instalando o Inboxer"
wget https://github.com/denysdovhan/inboxer/releases/download/v1.3.2/inboxer-1.3.2-x86_64.AppImage -O inboxer.AppImage
chmod +x inboxer.AppImage
sudo mv inboxer.AppImage /usr/local/bin/inboxer.AppImage
touch ~/.local/share/applications/inboxer.desktop
echo "[Desktop Entry]
Name=Inboxer
Comment=Google Mail Software
Exec=/usr/local/bin/inboxer.AppImage
Icon=/home/$USER/.icons/mail.png
Terminal=false
Type=Application
Categories=Development" >> ~/.local/share/applications/inboxer.desktop

echo "Instalando Thunderbird"
yes | sudo add-apt-repository ppa:lucid-bleed/ppa
sudo apt-get update
sudo apt-get install thunderbird thunderbird-locale-pt-br thunderbird-gnome-support -y

echo "Instalando Geary Mail"
yes |sudo add-apt-repository ppa:geary-team/releases
sudo apt-get update
sudo apt install geary -y

sudo snap install dynamodb-gui-client

sudo snap install istekram

echo "Limpando"
sudo rm -fr /tmp/*
sudo apt-get autoremove 
sudo apt-get clean all

echo "Criando pastas com icones"
mkdir ~/Área\ de\ Trabalho/github
mkdir ~/Área\ de\ Trabalho/gitlab
mkdir ~/Área\ de\ Trabalho/devops
mkdir ~/Área\ de\ Trabalho/uudi

gio set -t string ~/Área\ de\ Trabalho/github/ metadata::custom-icon file:///home/david/.icons/github.png
gio set -t string ~/Área\ de\ Trabalho/gitlab/ metadata::custom-icon file:///home/david/.icons/gitlab.png
gio set -t string ~/Área\ de\ Trabalho/devops/ metadata::custom-icon file:///home/david/.icons/devops.png
gio set -t string ~/Área\ de\ Trabalho/uudi/ metadata::custom-icon file:///home/david/.icons/uudi.png

sync
